//
//  Draw.swift
//  Swift Graphics
//
//  Created by Parankush Bhardwaj on 8/28/18.
//  Copyright © 2018 Parankush Bhardwaj. All rights reserved.
//

import Cocoa

class Draw: NSView {
    
    func drawPoint(in context: CGContext, color: CGColor, xPosition: Double, yPosition: Double) {
        
        let p1 = CGPoint(x: xPosition, y: yPosition)
        let p2 = CGPoint(x: xPosition + 0.25, y: yPosition + 0.25)
        
        context.setLineWidth(0.5)
        context.setStrokeColor(color)
        context.beginPath()
        context.move(to: p1)
        context.addLine(to: p2)
        context.strokePath()
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        window?.backgroundColor = NSColor.white
        
        let context = NSGraphicsContext.current?.cgContext
        context?.setShouldAntialias(false)
        
        drawPoint(in: context!, color: NSColor.black.cgColor, xPosition: 100, yPosition: 100)
    }
}
