//
//  MainWindowController.swift
//  Swift Graphics
//
//  Created by Parankush Bhardwaj on 8/28/18.
//  Copyright © 2018 Parankush Bhardwaj. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController {

    convenience init() {
        self.init(windowNibName: "MainWindowController")
    }
    
    override func windowDidLoad() {
        super.windowDidLoad()

        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        window?.titlebarAppearsTransparent = true
        window?.titleVisibility = .hidden
    }
    
}
